const io = require("socket.io-client");
const socket = io("http://localhost:1503");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let username = null;
let password = null;

const privateRegex = /^chat;([A-Z0-9]+);(.+)/i; // for sending privately to someone
const groupRegex = /^group;([A-Z0-9]+);(.+)/i; // for group chat
const inviteRegex = /^invite;([A-Z0-9]+);(.+)/i; // for inviting someone to the group

const chatAllCmd = "chat_all;"; // for sending message to everyone
const createOrJoinGroupCmd = "jcg;"; // for creating or joining a group

console.log("Connecting to the server...");
socket.on("connect", () => {
  username = process.argv[2];
  password = process.argv[3];
  console.log(`Welcome ${username}!`);
  socket.emit("packet", {
    sender: username,
    action: "join",
    password: password,
  });
});

socket.on("disconnect", (reason) => {
  console.log(`Server disconnected, reason: ${reason}`);
  rl.close();
});

rl.on("line", (input) => {
  if (privateRegex.test(input)) {
    const info = input.match(privateRegex);
    socket.emit("packet", {
      sender: username,
      action: "send",
      dest: info[1],
      msg: info[2],
    });
  } else if (input.startsWith(chatAllCmd)) {
    const str = input.slice(chatAllCmd.length);
    socket.emit("packet", { sender: username, action: "broadcast", msg: str });
  } else if (input.startsWith(createOrJoinGroupCmd)) {
    const str = input.slice(createOrJoinGroupCmd.length);
    socket.emit("packet", { sender: username, action: "jgroup", group: str });
  } else if (groupRegex.test(input)) {
    const info = input.match(groupRegex);
    socket.emit("packet", {
      sender: username,
      action: "group",
      group: info[1],
      msg: info[2],
    });
  } else if (inviteRegex.test(input)) {
    const info = input.match(inviteRegex);
    socket.emit("packet", {
      sender: username,
      action: "invite",
      group: info[1],
      dest: info[2],
    });
  }
});

socket.on("packet", (data) => {
  switch (data.action) {
    case "join":
      console.log(`${data.sender} has joined the chat`);
      break;
    case "send":
      console.log(`${data.sender}: ${data.msg}`);
      break;
    case "broadcast":
      console.log(`${data.sender} to everyone: ${data.msg}`);
      break;
    case "group":
      console.log(`${data.sender} to ${data.group}: ${data.msg}`);
      break;
    case "invite":
      console.log(`${data.sender} has invited you to group ${data.group}`);
    default:
      break;
  }
});
