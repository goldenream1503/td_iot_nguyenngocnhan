const expect = require("chai").expect;
const io = require("socket.io-client");
let tests = 1;

describe("----Testing Server in the chat", function () {
  let socket_1 = null, socket_2 = null;
  const username_1 = "test_nhann";
  const username_2 = "test_huyentt";
  const password = "1234";

  beforeEach(function (done) {
    socket_1 = io("http://localhost:1503");
    socket_2 = io("http://localhost:1503");
    console.log(`>> Test #${tests++}`);
    socket_1.on("connect", () => {
      console.log("socket_1 connected");
      socket_1.emit("packet", {
        sender: username_1,
        action: "join",
        password: password,
      });
    });
    socket_2.on("connect", () => {
      console.log("socket_2 connected");
      socket_2.emit("packet", {
        sender: username_2,
        action: "join",
        password: password,
      });
    });

    socket_1.on("disconnect", function () {
      console.log("socket_1 disconnected");
    });

    socket_2.on("disconnect", function () {
      console.log("socket_2 disconnected");
    });
    done();
  });

  afterEach(function (done) {
    socket_1.disconnect();
    socket_2.disconnect();
    done();
  });

  it("Notify that a user joined the chat", (done) => {
    socket_1.on("packet", (data) => {
      expect(data.action).to.equal("join");
      done();
    });
  });

  it("Broadcast a message to others in the chat", function (done) {
    var msg_hello = "hello socket_2";
    socket_1.emit("packet", {
      sender: username_1,
      action: "broadcast",
      msg: msg_hello,
    });
    socket_2.on("packet", function (data) {
      if (data.action === "broadcast") {
        expect(data.msg).to.equal(msg_hello);
        done();
      }
    });
  });

  it("Send a message secretly to someone", function (done) {
    var msg_hello = "hello socket_2";
    socket_1.emit("packet", {
      sender: username_1,
      action: "send",
      dest: username_2,
      msg: msg_hello,
    });
    socket_2.on("packet", function (data) {
      if (data.action === "send") {
        expect(data.dest).to.equal(username_2);
        expect(data.msg).to.equal(msg_hello);
        done();
      }
    });
  });
});

describe("----Testing Server in a group", function () {
  let socket_1 = null, socket_2 = null, socket_3 = null;
  const username_1 = "test_nhann";
  const username_2 = "test_huyentt";
  const username_3 = "John";
  const group_name = "minf19";
  const password = "1234";

  beforeEach(function (done) {
    socket_1 = io("http://localhost:1503");
    socket_2 = io("http://localhost:1503");
    socket_3 = io("http://localhost:1503");

    console.log(`>> Test #${tests++}`);
    socket_1.on("connect", function () {
      console.log("socket_1 connected...");
      socket_1.emit("packet", {
        sender: username_1,
        action: "join",
        password: password,
      });
      socket_1.emit("packet", {
        sender: username_1,
        action: "jgroup",
        group: group_name,
      });
    });

    socket_2.on("connect", function () {
      console.log("socket_2 connected...");
      socket_2.emit("packet", {
        sender: username_2,
        action: "join",
        password: password,
      });
      socket_2.emit("packet", {
        sender: username_2,
        action: "jgroup",
        group: group_name,
      });
    });

    socket_3.on("connect", () => {
      console.log("socket_3 connected");
      socket_3.emit("packet", {
        sender: username_3,
        action: "join",
        password: password,
      });
      
    });

    socket_1.on("disconnect", function () {
      console.log("socket_1 disconnected");
    });

    socket_2.on("disconnect", function () {
      console.log("socket_2 disconnected");
    });

    socket_3.on("disconnect", function () {
      console.log("socket_3 disconnected");
    });
    done();
  });

  afterEach(function (done) {
    socket_1.disconnect();
    socket_2.disconnect();
    socket_3.disconnect();
    done();
  });

  it("Notify that a user joined a group", function (done) {
    let runTime = 0;
    socket_1.on("packet", function (data) {
      if (data.action === "jgroup" && runTime === 0) {
        expect(data.sender).to.equal(username_2);
        expect(data.group).to.equal(group_name);
        runTime++; // Prevent asynchronously run multiple done() <-- this will cause exception
        done();
      }
    });
  });

  it("Broadcast a message to a group", function (done) {
    var msg_hello = "hello socket_2";
    
    socket_1.on("packet", function (data) {
      if (data.action === "jgroup" && data.sender === username_2) {
        socket_1.emit("packet", {
          sender: username_1,
          action: "group",
          group: group_name,
          msg: msg_hello,
        });
      }
    });
    let runTime = 0;
    socket_2.on("packet", function (data) {
      if (data.action === "group" && runTime === 0) {
        expect(data.group).to.equal(group_name);
        expect(data.msg).to.equal(msg_hello);
        runTime++; // Prevent asynchronously run multiple done() <-- this will cause exception
        done();
      }
    });
  });

  it("Invite a new user to group", function (done) {
    socket_1.emit("packet", {
      sender: username_1,
      action: "invite",
      dest: username_3,
      group: group_name,
    });

    socket_3.on("packet", (data) => {
      if (data.action === "invite") {
        expect(data.sender).to.equal(username_1);
        expect(data.dest).to.equal(username_3);
        expect(data.group).to.equal(group_name);
        done();
      }
    });
  });
});
