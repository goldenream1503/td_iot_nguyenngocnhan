const elasticsearch = require("@elastic/elasticsearch");
const bcrypt = require("bcrypt");

const port = 1503;
const socketio = require("socket.io")(port);
const elastic = new elasticsearch.Client({ node: "http://localhost:9200" });
const saltRounds = 10;

console.log(`Server is running at localhost:${port}`);

socketio.on("connect", (socket) => {
  console.log("A user connected");
  socket.on("packet", (data) => {
    switch (data.action) {
      case "join":
        console.log("Username: ", data.sender, ", ID: ", socket.id);
        console.log(
          `Number of users: ${socketio.of("/").server.engine.clientsCount}`
        );

        elastic.search(
          {
            index: "accounts",
            body: {
              query: {
                match: {
                  username: data.sender,
                },
              },
            },
          },
          (err, result) => {
            if (err) console.log(err);
            if (
              404 === result.statusCode ||
              (200 === result.statusCode && null === result.body.hits.max_score)
            ) {
              if (404 === result.statusCode) {
                console.log("Index not found!");
              } else {
                console.log("Username not found!");
              }

              bcrypt.hash(data.password, saltRounds, function (
                err,
                hashed_password
              ) {
                elastic.create(
                  {
                    id: new Date().getTime(),
                    index: "accounts",
                    refresh: "true",
                    body: {
                      username: data.sender,
                      password: hashed_password,
                    },
                  },
                  (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                      console.log("Saved the account to the database");
                      socket.broadcast.emit("packet", data);
                    }
                  }
                );
              });
            } else {
              bcrypt.compare(
                data.password,
                result.body.hits.hits[0]._source.password,
                function (err, result) {
                  if (result) {
                    console.log("Correct password");
                    socket.broadcast.emit("packet", data);
                  } else {
                    console.log("Incorrect password!");
                    socket.disconnect(true);
                  }
                }
              );
            }
          }
        );
        socket.username = data.sender;
        break;

      case "send":
        let socket_id = null;
        for (var key in socketio.of("/").sockets) {
          if (
            data.dest === socketio.of("/").sockets[key].username
          ) {
            socket_id = socketio.of("/").sockets[key].id;
          }
        }

        if (socket_id !== null) {
          socketio.to(socket_id).emit("packet", data);
          elastic.create(
            {
              id: new Date().getTime(),
              index: "private_messages",
              refresh: "true",
              body: {
                group: socket.id,
                sender: data.sender,
                receiver: data.dest,
                msg: data.msg,
              },
            },
            (err, result) => {
              if (err) console.log(err);
              if (201 === result.statusCode) {
                console.log("Saved the message to the database");
              }
            }
          );
        }
        break;
      case "broadcast":
        socket.broadcast.emit("packet", data);
        elastic.create(
          {
            id: new Date().getTime(),
            index: "broadcast_messages",
            refresh: "true",
            body: {
              group: socket.id,
              sender: data.sender,
              msg: data.msg,
            },
          },
          (err, result) => {
            if (err) console.log(err);
            if (201 === result.statusCode) {
              console.log("Saved the message to the database");
            }
          }
        );
        break;
      case "jgroup":
        socket.join(data.group, () => {
          console.log(`Group: ${data.group}, joined: ${data.sender}`);
          socket.to(data.group).emit("packet", data);
          socket.broadcast.emit("packet", data);
        });
        break;
      case "group":
        socket.to(data.group).emit("packet", data);
        elastic.create(
          {
            id: new Date().getTime(),
            index: "group_messages",
            refresh: "true",
            body: {
              group: data.group,
              sender: data.sender,
              msg: data.msg,
            },
          },
          (err, result) => {
            if (err) console.log(err);
            if (201 === result.statusCode) {
              console.log("Saved the message to the database");
            }
          }
        );
        break;
      case "invite": {
        let socket_id = null;
        for (var key in socketio.of("/").sockets) {
          if (
            data.dest === socketio.of("/").sockets[key].username
          ) {
            socket_id = socketio.of("/").sockets[key].id;
          }
        }
        if (socket_id != null) {
          socketio.sockets.connected[socket_id].join(data.group);
          socketio.to(socket_id).emit("packet", data);
          console.log(
            `${data.sender} has invited ${data.dest} to ${data.group}`
          );
        }
        break;
      }
      default:
        break;
    }
  });

  socket.on("disconnect", (reason) => {
    console.log(`A user has disconnected, reason: ${reason}`);
    console.log(
      `Number of users left: ${socketio.of("/").server.engine.clientsCount}`
    );
  });
});
